FROM debian:buster

EXPOSE 80 443 

RUN apt update && apt install -y wget \
    nano \
    net-tools\
    curl

RUN apt update && apt install -y ca-certificates apt-transport-https\
    gnupg\
    gnupg2\
    gnupg1

RUN wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add -
RUN echo "deb https://packages.sury.org/php/ buster main" | tee /etc/apt/sources.list.d/php.list

RUN apt update && apt install -y \
    apache2\
    php7.3 php7.3-mysql php7.3-dom php7.3-simplexml php7.3-ssh2 php7.3-xml php7.3-xmlreader php7.3-curl  php7.3-exif  php7.3-ftp php7.3-gd  php7.3-iconv php7.3-imagick php7.3-json  php7.3-mbstring php7.3-posix php7.3-sockets php7.3-tokenizer

# RUN apt --install-recommends --install-suggests install -y \
#     php7.1 php7.1-cli php7.1-common php7.1-curl php7.1-mbstring php7.1-mysql php7.1-xml

# RUN apt install -y \
#     php7.1 php7.1-cli php7.1-common php7.1-curl php7.1-mbstring php7.1-mysql php7.1-xml

RUN echo "ServerName 127.0.0.1" >> /etc/apache2/apache2.conf
# ADD /CONF_WP/Apache_conf/apache2.conf /etc/apache2/

RUN curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
RUN apt install -y nodejs

# funciona no dockerfile e no composer
# ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]

# VOLUME [ "/etc/apache2/" ]
VOLUME [ "/etc/apache2/sites-available/" ]
VOLUME [ "/var/www/html" ]

WORKDIR /var/www/html
